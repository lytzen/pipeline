//
//  ViewController.swift
//  Pipeline
//
//  Created by Michael Lytzen Hansen on 03/07/2018.
//  Copyright © 2018 Michael Lytzen Hansen. All rights reserved.
//

import Cocoa
import MetalKit

class ViewController: NSViewController {

	var renderer: Renderer?

	override func viewDidLoad() {
		guard let metalView = view as? MTKView else {
			fatalError("Metal view not set up in storyboard")
		}
		renderer = Renderer(metalView: metalView)
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}


}

