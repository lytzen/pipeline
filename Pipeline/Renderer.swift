//
//  Renderer.swift
//  Pipeline
//
//  Created by Michael Lytzen Hansen on 03/07/2018.
//  Copyright © 2018 Michael Lytzen Hansen. All rights reserved.
//

import Foundation
import MetalKit

class Renderer: NSObject {
	static var device: MTLDevice!
	static var commandQueue: MTLCommandQueue!
	var mesh: MTKMesh!
	var vertexBuffer: MTLBuffer!
	var pipelineState: MTLRenderPipelineState!

	var timer: Float = 0

	init(metalView: MTKView) {
		// MARK: - Initialize the GPU and create the command queue.
		guard let device = MTLCreateSystemDefaultDevice() else {
			fatalError("GPU not available")
		}
		metalView.device = device
		Renderer.commandQueue = device.makeCommandQueue()!

		// MARK: - initialize a cube primitive
		//let mdlMesh = Primitive.makeCube(device: device, size: 1)
		guard let mdlMesh = Primitive.makeMeshFromObj(device: device, filename: "train", withExtension: "obj") else {
			fatalError("Unable to create mesh from object file")
		}
		// Convert the MDLMesh to an MTKMesh for use in Metal.
		do {
			mesh = try MTKMesh(mesh: mdlMesh, device: device)
		} catch let error {
			print(error.localizedDescription)
		}

		// Set up the MTLBuffer that contains the vertex data to send to the GPU.
		vertexBuffer = mesh.vertexBuffers[0].buffer

		// MARK: - Setup the MTLLibrary
		let library = device.makeDefaultLibrary()
		let vertexFunction = library?.makeFunction(name: "vertex_main")
		let fragmentFunction = library?.makeFunction(name: "fragment_main")

		// MARK: - Setup the pipeline state
		let pipelineDescriptor = MTLRenderPipelineDescriptor()
		pipelineDescriptor.vertexFunction = vertexFunction
		pipelineDescriptor.fragmentFunction = fragmentFunction
		pipelineDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mdlMesh.vertexDescriptor)
		pipelineDescriptor.colorAttachments[0].pixelFormat = metalView.colorPixelFormat
		do {
			pipelineState = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
		} catch let error {
			fatalError(error.localizedDescription)
		}
		super.init()

		metalView.clearColor = MTLClearColor(red: 1.0, green: 1.0, blue: 0.8, alpha: 1.0)
		metalView.delegate = self
	}
}

// MARK: - <#MTKViewDelegate#>
// Make the Renderer conform to MTKViewDelegate, so it can be called every frame.
extension Renderer:MTKViewDelegate {
	func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
	}

	func draw(in view: MTKView) {
		guard let descriptor = view.currentRenderPassDescriptor,
		let commandBuffer = Renderer.commandQueue.makeCommandBuffer(),
			let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: descriptor) else {
				return
		}

		// MARK: - Draw code

		timer += 0.05	// Update the time every frame
		var currentTime = sin(timer)	// The time will be between -1 and 1 (for moving up and down)
		renderEncoder.setVertexBytes(&currentTime, length: MemoryLayout<Float>.stride, index: 1)	// Send the time to the vertex shader's byte buffer.

		renderEncoder.setRenderPipelineState(pipelineState)
		renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
		for submesh in mesh.submeshes {
			renderEncoder.drawIndexedPrimitives(type: .triangle,
																					indexCount: submesh.indexCount,
																					indexType: submesh.indexType,
																					indexBuffer: submesh.indexBuffer.buffer,
																					indexBufferOffset: submesh.indexBuffer.offset)
		}
		renderEncoder.endEncoding()
		guard let drawable = view.currentDrawable else {
			return
		}
		commandBuffer.present(drawable)
		commandBuffer.commit()
	}
}
