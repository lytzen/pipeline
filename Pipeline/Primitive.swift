//
//  Primitive.swift
//  Pipeline
//
//  Created by Michael Lytzen Hansen on 03/07/2018.
//  Copyright © 2018 Michael Lytzen Hansen. All rights reserved.
//

import Foundation
import MetalKit

class Primitive {

	/// Class method for making a equal sided cube mesh
	///
	/// - Parameters:
	///   - device: The MTLDevice
	///   - size: The size of the sides
	/// - Returns: The cube as an MDLMesh
	class func makeCube(device: MTLDevice, size: Float) -> MDLMesh {
		let allocator = MTKMeshBufferAllocator(device: device)
		let mesh = MDLMesh(boxWithExtent: [size, size, size],
											 segments: [1, 1, 1],
											 inwardNormals: false,
											 geometryType: .triangles,
											 allocator: allocator)
		return mesh

	}

	class func makeMeshFromObj(device: MTLDevice, filename:String, withExtension:String) -> MDLMesh? {
		guard let assetURL = Bundle.main.url(forResource: filename, withExtension: withExtension) else {
			return nil
		}
		let vertexDescriptor = MTLVertexDescriptor()
		vertexDescriptor.attributes[0].format = .float3
		vertexDescriptor.attributes[0].offset = 0
		vertexDescriptor.attributes[0].bufferIndex = 0
		vertexDescriptor.layouts[0].stride = MemoryLayout<float3>.stride
		let meshDescriptor = MTKModelIOVertexDescriptorFromMetal(vertexDescriptor)
		(meshDescriptor.attributes[0] as! MDLVertexAttribute).name = MDLVertexAttributePosition

		let allocator = MTKMeshBufferAllocator(device: device)

		let asset = MDLAsset(url: assetURL, vertexDescriptor: meshDescriptor, bufferAllocator: allocator)
		return asset.object(at: 0) as? MDLMesh
	}
}
