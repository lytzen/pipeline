//
//  Shaders.metal
//  Pipeline
//
//  Created by Michael Lytzen Hansen on 03/07/2018.
//  Copyright © 2018 Michael Lytzen Hansen. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

// Struct describing the vertex attributes that match the vertex descriptor.
struct VertexIn {
	float4 position [[ attribute(0) ]];
};

// Vertex shader, which takes the VertexIn, and returns its position. The vertex shader gets the current index via the [[ stage_in ]] attribute and unpacks the VertexIn struct cached for the vertex at the current index.
vertex float4 vertex_main(const VertexIn vertexIn [[ stage_in ]], constant float &timer [[ buffer(1) ]]) {
	float4 position = vertexIn.position;
	position.x += timer;
	return position;
}

fragment float4 fragment_main() {
	return float4(0, 0, 1, 1);
}
